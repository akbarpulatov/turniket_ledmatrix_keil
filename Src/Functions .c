#include "main.h"
#include "Functions.h"
#include "checksum.h"
#include "Font.h"
#include "unittests.h"

//============= < TOP LEVEL AND HARDWARE CALLBACK FUNCTIONS > ====================================================================
inline void myInit()		//Function which is called from main.c before loop for initializing
{
	LineContrTemp._word = 0x0101;
	DelDemo = 2000;
	DelPeriodCheck = _DelInitCheck;
}

inline void myMainLoop() 	//Loop which is called from Main loop of main.c
{
	DispKrest();
	DispError();
	DispHourGlass();
	DispNumber();
	DispStrelka();
	DispPeriodCheck();
	Display_horizontalshift();
}

void HAL_IncTick(void){
	//__unitT_HardwareTimer();
	updateTimers();
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){	// Interrupts from Timers if(htim->Instance == TIM3)		//Interrupt from Timer_3
	if(htim->Instance == TIM3){
		//__unitT_HardwareTimer();
	}
	else if(htim->Instance == TIM4)	//Interrupt from Timer_4 
	{
		//__unitT_HardwareTimer();		
		pseudo_thread4_UpdDisp();
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)	//External Interrupt Function
{
	if (GPIO_Pin == CS_pin)		//Interrupt from CS pin, (Falling Edge)
	{
		messStart();
	}
	else if (GPIO_Pin == CLK_pin)	//Interrupt from CLK pin, (Falling Edge)
	{
		messRead();
	}
}

void myErrorHandler(BYTE ErrorID)
{
	
}

//====================================== < HIGH LEVEL FUNCTIONS > ================================================================
inline void pseudo_thread4_UpdDisp(void)
{
	if (LineContrTemp.b15)
	{
		LineContrTemp._word = 0x0101;
		CountLine = 0;
	}
	else
	{
		LineContrTemp._word = (LineContrTemp._word << 1);
		CountLine++;
	}
	LineContr.b0 = LineContrTemp.b7;
	LineContr.b1 = LineContrTemp.b6;
	LineContr.b2 = LineContrTemp.b5;
	LineContr.b3 = LineContrTemp.b4;

	LineContr.b4 = LineContrTemp.b0;
	LineContr.b5 = LineContrTemp.b1;
	LineContr.b6 = LineContrTemp.b2;
	LineContr.b7 = LineContrTemp.b3;

	LineContr.b8 = LineContrTemp.b15;
	LineContr.b9 = LineContrTemp.b14;
	LineContr.b10 = LineContrTemp.b13;
	LineContr.b11 = LineContrTemp.b12;

	LineContr.b12 = LineContrTemp.b8;
	LineContr.b13 = LineContrTemp.b9;
	LineContr.b14 = LineContrTemp.b10;
	LineContr.b15 = LineContrTemp.b11;

	ShiftLed(CountLine);
}

//-------------------------------------- < Led Matrix States > -------------------------------------------------------------------
void DisplayDemo()
{
	if(ModeDemo) 
	{

	}

}

void DispKrest() //Krest symbolni boshqarish programmasi
{
	if ((StateDisplay & _StateDispMask) == _StateDispKrest)
	{
		if (Del_BlinkKrest == 0)
		{
			if (!FlagKrest)
			{
				SendToBuffer(Shape_AllOFF, Shape_Krest);
				FlagKrest = 1;
			}
			else
			{
				SendToBuffer(Shape_AllOFF, Shape_AllOFF);
				FlagKrest = 0;
			}
			Del_BlinkKrest = 500;
		}
	}
	else
	{
		Del_BlinkKrest = 0;
		FlagKrest = 0;
	}
}

void DispError() //Error Symbol uchun javob beradi
{
	if (((StateDisplay & _StateDispMask) >= _stateDispE0) && ((StateDisplay & _StateDispMask) <= _stateDispE9))
	{
		if (Del_BlinkError == 0)
		{
			if (!FlagError)
			{
				SendToBuffer(Shape_AllOFF, Shape_Error(StateDisplay & 0x000F));
				FlagError = 1;
			}
			else
			{
				SendToBuffer(Shape_AllOFF, Shape_Krest);
				FlagError = 0;
			}
			Del_BlinkError = 500;
		}
	}
	else
	{
		Del_BlinkError = 0;
		FlagError = 0;
	}
}

void DispHourGlass()
{
	if ((StateDisplay & _StateDispMask) == _StateDispHourGlass)
	{
		switch (CountRotate)
		{
			case 0:		SendToBuffer(Shape_Pourglass1G, Shape_Pourglass1R); break;
			case 1:		SendToBuffer(Shape_Pourglass2G, Shape_Pourglass2R); break;
			case 2:		SendToBuffer(Shape_Pourglass3G, Shape_Pourglass3R); break;
			case 3:		SendToBuffer(Shape_Pourglass4G, Shape_Pourglass4R); break;
		}
	}
	else
	{
		CountRotate = 0;
		DelRotate = 0;
	}
}

void DispStrelka()
{
	if ((StateDisplay & _StateDispMask) == _StateDispStrelka)
	{
		if (!FlagStrelka)
		{
			for (int i = 0; i < 16; i++)
			{
				BuffShift[i] = Shape_Strelka[i];
			}
			FlagStrelka = 1;
		}

		if (DelShift == 0)
		{
			for (int i = 16; i > 0; i--)
			{
				BuffShift[i] = BuffShift[i - 1];
			}
			BuffShift[0] = BuffShift[16];

			SendToBuffer(BuffShift, Shape_AllOFF);
			DelShift = 50;
		}
	}
	else
	{
		FlagStrelka = 0;
		DelShift = 0;
	}
}

void DispNumber()  
{
	if ((StateDisplay & _StateDispMask) >= 0 && (StateDisplay & _StateDispMask) < 101)
	{
		
		//Agar yosh bola bo`lsa Sariq rangda, agar katta bo`lsa yashil rangda chiqaradi sonni
		if(((StateDisplay & _StateTicketTypeMask) == _StateTicketTypeTB) || ((StateDisplay & _StateTicketTypeMask) == _StateTicketTypeMB))
		{
			SendToBuffer(Shape_AllOFF, Shape_2digits(StateDisplay));	//qizil
		}
		else if(((StateDisplay & _StateTicketTypeMask) == _StateTicketTypeTK) || ((StateDisplay & _StateTicketTypeMask) == _StateTicketTypeMK))
		{
			SendToBuffer(Shape_2digits(StateDisplay), Shape_AllOFF);	//yashil
		}

		//Sonni ko`rsatgandan keyin avval taymer qo`yamiz son ko`rsatilgani haqida flag qo`yamiz, va taymer o`tgandan keyin strelkaga/krestga o`tadi
		if (DelNumber == 0)
		{
			if (!FlagNumber)
			{
				FlagNumber = 1;
				DelNumber = 1000;
			}
			else if ((StateDisplay & _StateDispMask) > 0)
			{
				StateDisplay = (StateDisplay & ~_StateDispMask) | _StateDispStrelka;	//Strelka qo`yamiz DelNumber taymer tugagandan keyin
				FlagNumber = 0;
				//				FlagNewMessage = 0;

			}
			else	//StateDisplay == 0 ga teng bo`lsa shu shartga kiradi
			{
				StateDisplay = _StateDispKrest;
				FlagNumber = 0;
				//				FlagNewMessage = 0;
			}
		}
	}
	else //Agar StateDisplay o`zgarsa unda Flag va Taymerlarni default qiymatiga qaytaramiz
	{
		DelNumber = 0;
		FlagNumber = 0;
	}
}

void DispPeriodCheck()
{
	if (DelPeriodCheck == 0)
	{
		StateDisplay = _StateDispConnErr;
	}

	if (StateDisplay == _StateDispConnErr)
	{
		if (DelBlinkConnErr == 0)
		{
			if (!FlagConnError)
			{
				SendToBuffer(Shape_AllOFF, Shape_Error(0));
				FlagConnError = 1;
			}
			else
			{
				SendToBuffer(Shape_AllOFF, Shape_Krest);
				FlagConnError = 0;
			}
			DelBlinkConnErr = 500;
		}
	}
	else
	{
		FlagConnError = 0;
		DelBlinkConnErr = 0;
	}
}

void Display_horizontalshift()
{
	if (StateDisplay == _StateDispXodim)
	{

		if (!FlagHorShift)
		{
			for (BYTE i = 0; i < 16; i++)
			{
				BufferShiftHor[i] = Shape_32Xodim[i];
				SendShiftedToBuffer(Shape_32Xodim, Shape_32AllOFF);
			}
			FlagHorShift = 1;
		}

		if (DelHorShift == 0 && FlagHorShift)
		{
			for (BYTE j = 0; j < 16; j++)
			{
				BufferShiftHor[j] = (BufferShiftHor[j] >> 1);
				//BufferShiftHor[j] =((BufferShiftHor[j] >> 1) | ((BufferShiftHor[j] & Lbit) << 31));

			}
			SendShiftedToBuffer(BufferShiftHor, BufferShiftHor);
			CountHorShift++;
			DelHorShift = 110;
		}

		if (CountHorShift > 22)
		{
			CountHorShift = 0;
			StateDisplay = _StateDispStrelka;
			FlagHorShift = 0;
			DelHorShift = 0;
		}

	}
	else
	{

	}
}


//====================================== < MID LEVEL FUNCTIONS > =================================================================
inline WORD* Shape_2digits(WORD m) // n soni qabul qiladi va WORD[16] massivga pointerni qaytaradi
{
	BYTE n = m & 0x7F;
	
	if (n < 10)	//0-9gacha sonlar
	{
		for (int i = 0; i < 16; i++)
		{
			switch (n % 10)
			{
				case 0:		BuiltNumbShape[i] = Shape_centerdigit_0[i+2]; break;
				case 1:		BuiltNumbShape[i] = Shape_centerdigit_1[i+2]; break;
				case 2:		BuiltNumbShape[i] = Shape_centerdigit_2[i+2]; break;
				case 3:		BuiltNumbShape[i] = Shape_centerdigit_3[i+2]; break;
				case 4:		BuiltNumbShape[i] = Shape_centerdigit_4[i+2]; break;
				case 5:		BuiltNumbShape[i] = Shape_centerdigit_5[i+2]; break;
				case 6:		BuiltNumbShape[i] = Shape_centerdigit_6[i+2]; break;
				case 7:		BuiltNumbShape[i] = Shape_centerdigit_7[i+2]; break;
				case 8:		BuiltNumbShape[i] = Shape_centerdigit_8[i+2]; break;
				case 9:		BuiltNumbShape[i] = Shape_centerdigit_9[i+2]; break;
			}
		}
	}
	else if (n < 100)	//10-99 gacha sonlar
	{
		for (BYTE i = 0; i < 16; i++)
		{
			switch (n % 10)
			{
				case 0:		BuiltNumbShape[i] = Shape_rightdigit_0[i+1]; break;
				case 1:		BuiltNumbShape[i] = Shape_rightdigit_1[i+1]; break;
				case 2:		BuiltNumbShape[i] = Shape_rightdigit_2[i+1]; break;
				case 3:		BuiltNumbShape[i] = Shape_rightdigit_3[i+1]; break;
				case 4:		BuiltNumbShape[i] = Shape_rightdigit_4[i+1]; break;
				case 5:		BuiltNumbShape[i] = Shape_rightdigit_5[i+1]; break;
				case 6:		BuiltNumbShape[i] = Shape_rightdigit_6[i+1]; break;
				case 7:		BuiltNumbShape[i] = Shape_rightdigit_7[i+1]; break;
				case 8:		BuiltNumbShape[i] = Shape_rightdigit_8[i+1]; break;
				case 9:		BuiltNumbShape[i] = Shape_rightdigit_9[i+1]; break;
			}
			switch ((n % 100) / 10)
			{
				case 0:		BuiltNumbShape[i] |= Shape_rightdigit_0[i+1] >> 6; break;
				case 1:		BuiltNumbShape[i] |= Shape_rightdigit_1[i+1] >> 5; break;
				case 2:		BuiltNumbShape[i] |= Shape_rightdigit_2[i+1] >> 6; break;
				case 3:		BuiltNumbShape[i] |= Shape_rightdigit_3[i+1] >> 6; break;
				case 4:		BuiltNumbShape[i] |= Shape_rightdigit_4[i+1] >> 6; break;
				case 5:		BuiltNumbShape[i] |= Shape_rightdigit_5[i+1] >> 6; break;
				case 6:		BuiltNumbShape[i] |= Shape_rightdigit_6[i+1] >> 6; break;
				case 7:		BuiltNumbShape[i] |= Shape_rightdigit_7[i+1] >> 6; break;
				case 8:		BuiltNumbShape[i] |= Shape_rightdigit_8[i+1] >> 6; break;
				case 9:		BuiltNumbShape[i] |= Shape_rightdigit_9[i+1] >> 6; break;
			}
		}
	}
	else if (n == 100)	// 100 keladigan bo`lsa, uni 99+ deb ko`rsatadi
	{
		for (BYTE i = 0; i < 16; i++)
			BuiltNumbShape[i] = Shape_99plus[i];
	}
	
	//Sonning manfiy yoki musbatligi uchun oldiga '-' qo`yiladi, yoki qo`yilmaydi.
	if((StateDisplay & _StateNumSignMask) == _StateNumPos)
	{
		//Do nothing when positive
	}
	else if((StateDisplay & _StateNumSignMask) == _StateNumNeg)
	{
		//Bir xonali son bo`lsa
		if(n < 10)
		{
			for (BYTE i = 0; i < 16; ++i)
			{
				BuiltNumbShape[i] |= Shape_MinusSign[i]<<1; 
			}
		}
		else	//Son ikki xonali bo`lsa
		{
			for (BYTE i = 0; i < 16; ++i)
			{
				BuiltNumbShape[i+1] |= Shape_MinusSign[i] ; 
			}
		}
	}

	//Turistlar uchun T harfi qo`yiladi, Mahalliy xalq uchun esa T harfi bo`lmaydi.
	if(((m & _StateTicketTypeMask) == _StateTicketTypeTB) || ((m & _StateTicketTypeMask) == _StateTicketTypeTK)) {
		for (BYTE i = 0; i < 16; ++i)
		{
			BuiltNumbShape[i] |= Shape_TourSymb[i]; 
		}
	}

	return BuiltNumbShape;
}

static inline WORD* Shape_Error(WORD n) // n soni qabul qiladi va WORD[16] massivga pointerni qaytaradi
{
	for (BYTE i = 0; i < 16; i++)
	{

		switch (n % 10)
		{
			case 0:		BuiltErrShape[i] = Shape_rightdigit_0[i]; break;
			case 1:		BuiltErrShape[i] = Shape_rightdigit_1[i]; break;
			case 2:		BuiltErrShape[i] = Shape_rightdigit_2[i]; break;
			case 3:		BuiltErrShape[i] = Shape_rightdigit_3[i]; break;
			case 4:		BuiltErrShape[i] = Shape_rightdigit_4[i]; break;
			case 5:		BuiltErrShape[i] = Shape_rightdigit_5[i]; break;
			case 6:		BuiltErrShape[i] = Shape_rightdigit_6[i]; break;
			case 7:		BuiltErrShape[i] = Shape_rightdigit_7[i]; break;
			case 8:		BuiltErrShape[i] = Shape_rightdigit_8[i]; break;
			case 9:		BuiltErrShape[i] = Shape_rightdigit_9[i]; break;
		}

		BuiltErrShape[i] |= Shape_Errpic[i];
	}
	return BuiltErrShape;
}

static inline void R_G_ToLed(void)
{
	for (BYTE i = 0; i < 16; i++)
	{
		LedLine[i].b0 =  !BuffLineR[i].b3;
		LedLine[i].b1 =  !BuffLineG[i].b3;
		LedLine[i].b2 =  !BuffLineR[i].b2;
		LedLine[i].b3 =  !BuffLineG[i].b2;
		LedLine[i].b4 =  !BuffLineR[i].b1;
		LedLine[i].b5 =  !BuffLineG[i].b1;
		LedLine[i].b6 =  !BuffLineR[i].b0;
		LedLine[i].b7 =  !BuffLineG[i].b0;
		LedLine[i].b8 =  !BuffLineG[i].b4;
		LedLine[i].b9 =  !BuffLineR[i].b4;
		LedLine[i].b10 = !BuffLineG[i].b5;
		LedLine[i].b11 = !BuffLineR[i].b5;
		LedLine[i].b12 = !BuffLineG[i].b6;
		LedLine[i].b13 = !BuffLineR[i].b6;
		LedLine[i].b14 = !BuffLineG[i].b7;
		LedLine[i].b15 = !BuffLineR[i].b7;
		LedLine[i].b16 = !BuffLineR[i].b11;
		LedLine[i].b17 = !BuffLineG[i].b11;
		LedLine[i].b18 = !BuffLineR[i].b10;
		LedLine[i].b19 = !BuffLineG[i].b10;
		LedLine[i].b20 = !BuffLineR[i].b9;
		LedLine[i].b21 = !BuffLineG[i].b9;
		LedLine[i].b22 = !BuffLineR[i].b8;
		LedLine[i].b23 = !BuffLineG[i].b8;
		LedLine[i].b24 = !BuffLineG[i].b12;
		LedLine[i].b25 = !BuffLineR[i].b12;
		LedLine[i].b26 = !BuffLineG[i].b13;
		LedLine[i].b27 = !BuffLineR[i].b13;
		LedLine[i].b28 = !BuffLineG[i].b14;
		LedLine[i].b29 = !BuffLineR[i].b14;
		LedLine[i].b30 = !BuffLineG[i].b15;
		LedLine[i].b31 = !BuffLineR[i].b15;
	}
}

static inline void messStart(void)
{
	if (!Ser_CS){		//Beginning of the message, default values should be set
		count = 0;
		for (BYTE i=0; i<3; i++){ 
			tempmessage[i] = 0;
		}
		//printf("\nMessage Started, Count = %d", count);
	}
}

static inline void messRead(void)	//Reading of the message is started
{
	if (!Ser_CS && count != 24){
		tempmessage[count/8] |= ((Ser_DI) & 0x01) << ((count)%8);
		//printf("\nCount = %d, Message = %d", count, StateDisplay);
		count++;
	}

	
	if (count == 24)	// Agar hamma bitlar kelgan va opredelyonniy shartlar bajarilgan bo`lsa message ni yangilaymiz
	{
		messFinish();
	}
}

static inline void messFinish(void) 
{
	BYTE temp[3] = {tempmessage[0], tempmessage[1], tempmessage[2]};
	if (temp[2] == crc_8(temp, 2))	//Checking the CRC 
	{		
		if(((tempmessage[0] << 8 | tempmessage[1]) & _StateDispMask) != _IgnoreMessage){
		StateDisplay = (tempmessage[0] << 8 | tempmessage[1]);	// Message Global yangilanadigan nuqta
		count = 0;
		printf("\nSUCCESS! , Count = %d, Message = %X = %d, CRC = %X", count, StateDisplay, StateDisplay, tempmessage[2]);
		DelPeriodCheck = _DelPeriodCheck;
		}
		else
		{
			DelPeriodCheck = _DelPeriodCheck;
		}
	}
	else 
	{
		printf("\n!!!ERROR!!! COUNT = %d, Message = %X = %d, CRC = %X ", count, StateDisplay, StateDisplay, tempmessage[2]);
	}
}

static inline void updateTimers(void) 	//Updates the timers
{
	CountTick++;
	if (CountTick >= 500)
	{
		CountTick = 0;
		Tick ^= 1;
		LEDG = Tick;
	}
	
	Count100ms++;
	if (Count100ms >= 100)
	{
		Count100ms = 0;
		Tick100ms ^= 1;

		if (DelRotate == 0)
		{
			CountRotate++;
		}
		if (CountRotate > 3)
		{
			CountRotate = 0;
			DelRotate = 500;
		}
	}
	
	Count10ms++;
	if (Count10ms >= 10)
	{
		Tick10ms ^= 1;
		Count10ms = 0;
	}
	//------------------ Òàéìåðû -------------------------------------------------------------------------------------------
	if(msTMR_Del > 0)msTMR_Del--;
	if (msTMR1 > 0)msTMR1--;
	if (msTMR2 > 0)msTMR2--;
	if (msTMR3 > 0)msTMR3--;
	if (msTMR4 > 0)msTMR4--;
	if (msTMR5 > 0)msTMR5--;
	if (msTMR6 > 0)msTMR6--;
	if (msTMR7 > 0)msTMR7--;
	if (msTMR8 > 0)msTMR8--;
	
	
	if (msTMR11 > 0)msTMR11--;
	if (msTMR12 > 0)msTMR12--;
	if (msTMR13 > 0)msTMR13--;
	if (msTMR14 > 0)msTMR14--;
	if (msTMR15 > 0)msTMR15--;
	if (msTMR16 > 0)msTMR16--;
	if (msTMR17 > 0)msTMR17--;
	if (msTMR18 > 0)msTMR18--;
}

//====================================== < LOW LEVEL FUNCTIONS > =================================================================	
inline WORD DWordToWord_MSB(DWORD k)
{
	WORD p = k & 0x0000FFFF;
	return p;
}
inline void ShiftLed(BYTE line)	//Pushes the data to the Shift registers line by line
{
	BYTE digit;
	
	In_Led[line][0]._byte = LineContr.v[0];
	In_Led[line][1]._byte = LineContr.v[1];
	
	
	In_Led[line][2]._byte = LedLine[line].v[1];
	In_Led[line][3]._byte = LedLine[line].v[0];
	In_Led[line][4]._byte = LedLine[line].v[3];
	In_Led[line][5]._byte = LedLine[line].v[2];
	
	In_Led[line][6]._byte = LedLine[line + 8].v[1];
	In_Led[line][7]._byte = LedLine[line + 8].v[0];
	In_Led[line][8]._byte = LedLine[line + 8].v[3];
	In_Led[line][9]._byte = LedLine[line + 8].v[2];

	for (BYTE k = 10; k > 0; k--)
	{
		digit = In_Led[line][k - 1]._byte;
		for (BYTE i = 0; i < 8; i++)
		{
			DS_595 = digit & 0x01;
			digit = (digit >> 1);
			SH_595 = 1; 
			DelayNop10();
			DelayNop10();
			DelayNop10();
			DelayNop10();
			DelayNop10();
			SH_595 = 0;
		}
	}
	ST_595 = 1; 
	DelayNop10();
	DelayNop10();
	DelayNop10();
	DelayNop10();
	DelayNop10();
	ST_595 = 0;
}

struct __FILE { int handle; };
FILE __stduot;
FILE __stdin;

int fputc(int ch, FILE *f)
{
	if(DEMCR & TRCENA){
		while(ITM_Port32(0) == 0);
		ITM_Port8(0) = ch;		
	}
	return(ch);
}





