#ifndef __AKBARFUNCTIONS_H
#define __AKBARFUNCTIONS_H


//============================== < DEFINES OF FUNCTIONS.C > ===========================================================
#define SendToBuffer(g, r)					\
			for (int i = 0; i < 16; i++)	\
			{								\
				BuffLineG[i]._word = g[i];  \
				BuffLineR[i]._word = r[i];  \
			}                               \
			R_G_ToLed()
			
#define SendShiftedToBuffer(Green, Red)								\
			for (BYTE i = 0; i < 16; i++)					        \
			{                                                       \
				BuffLineG[i]._word = DWordToWord_MSB(Green[i]);     \
				BuffLineR[i]._word = DWordToWord_MSB(Red[i]);       \
			}                                                       \
			R_G_ToLed()
			
//============================== < VARIABLES TO FUNCTIONS.C > ===========================================================
//--------------------------------- < TIMERS > --------------------------------------------------------------------------
WORD msTMR_Del = 0;
WORD msTMR1;
WORD msTMR2;
WORD msTMR3;
WORD msTMR4;
WORD msTMR5;
WORD msTMR6;
WORD msTMR7;
WORD msTMR8;

WORD msTMR11;
WORD msTMR12;
WORD msTMR13;
WORD msTMR14 = 30000;
WORD msTMR15;
WORD msTMR16;
WORD msTMR17;
WORD msTMR18;

WORD CountTick = 0;
WORD Count10ms = 0;
WORD Count100ms = 0;
WORD Count1000ms = 0;
//--------------------------------- < Flags > ---------------------------------------------------------------------------
bool ENC_OK;
bool Tick10ms = 0;
bool Tick100ms = 0;


volatile Word Flags0;
volatile Word Flags1;
volatile Word Flags;
volatile BYTE tempflag = 0;
//--------------------------------- < EXTERNAL VARIABLES TO FUNCTIONS.C > -----------------------------------------------
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

//--------------------------------- < INTERNAL VARIABLES TO FUNCTIONS.C > -----------------------------------------------
// volatile BYTE my_Message[3] = {85, 85, 85};
volatile WORD StateDisplay = 0x007C;	//Initializing with state krest
static volatile BYTE tempmessage[3];
static volatile BYTE count;
volatile BYTE SerMessage;

volatile Byte In_Led[16][10];
volatile Dword LedLine[16];
volatile WORD BuffShift[17];
volatile Word BuffLineG[16];
volatile Word BuffLineR[16];
volatile Byte In_LedR[4][8];
volatile Byte In_LedG[4][8];
volatile Word LineContr;
volatile Word LineContrTemp;
volatile BYTE CountLine = 0;
volatile DWORD CountErrTX = 0;
volatile DWORD CountErrRX = 0;
volatile DWORD CountLed = 0;
volatile BYTE CountCAN = 0;
volatile BYTE CountTX_CAN = 0;
volatile Byte ErrByte;
volatile Byte ErrFlags;
volatile BYTE CountRotate = 0;
volatile BYTE myCountRotate = 0;

WORD BuiltNumbShape[16];
WORD BuiltErrShape[16];
DWORD BufferShiftHor[16];

bool FlagKrStr = 0;
bool SetKrStr = 1;
Byte Comm_IO;
BYTE CountHorShift = 0;



//============================== < FUNCTION PROTOTYPES OF FUNCTIONS.C > ==================================================
//---------------------------------- < HIGH LEVEL FUNCTIONS > ------------------------------------------------------------
void pseudo_thread4_UpdDisp(void);

void ReadMessage(void);
void DispKrest(void);
void DispError(void);
void DispHourGlass(void);
void DispNumber(void);
void DispStrelka(void);
void DispPeriodCheck(void);
void Display_horizontalshift(void);
void DisplayDemo(void);

//---------------------------------- < MID LEVEL FUNCTIONS > -------------------------------------------------------------
WORD* Shape_Error(WORD n);
WORD* Shape_2digits(WORD n);
void messStart(void);
void messRead(void);
void messFinish(void);

void updateTimers(void);

//---------------------------------- < LOW LEVEL FUNCTIONS > -------------------------------------------------------------
WORD DWordToWord_MSB(DWORD k);

//======================================== < CONSTANT VECTORS > ==========================================================



#endif // !__AKBARFUNCTIONS_H
